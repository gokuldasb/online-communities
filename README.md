FREE KNOWLEDGE COMMUNITIES
==========================

Knowledge comes alive in the community!

## Free Software Communities

1. Free Software Users' Group, Trivandrum (FSUG-TVM)
    - Website: http://tvm.fsug.in/
    - Matrix: https://matrix.to/#/#fsug-tvm:diasp.in
    - Telegram: https://t.me/fsugtvm

2. GNULinuxLovers
    - Matrix: https://matrix.to/#/#gnulinuxlovers:diasp.in
    - Telegram: https://t.me/GnuLinuxLovers

3. Free Software Community of India (FSCI)
    - Website: http://fsci.org.in/
    - Matrix: https://matrix.to/#/#fsci:poddery.com

## Software Development Communities

1. FSUG-TVM Developers (sub-group)
    - Matrix: https://matrix.to/#/#fsugtvmdev:tchncs.de

## Electronics Communities

1. EmbeddedIoTGeeks
    - Matrix: https://matrix.to/#/#embeddediotgeeks:diasp.in

## Mathematics Communities

1. MathLovers
    - Matrix: https://matrix.to/#/#mathlovers:diasp.in
    - Telegram: https://t.me/joinchat/AMM6t1KSUFI5qweuh27Zfg

## FOSS Digital Art Communities

1. FOSSArt
    - Matrix: https://matrix.to/#/#FOSSArt:diasp.in
    - Telegram: https://t.me/FOSSArt

## Astronomy Communities

1. Amateur Astronomers' Organisation (Aastro)
    - Website: http://www.aastro.in/
    - Telegram: https://t.me/aastrokerala

## Portfolio:  
- Website
- Telegram
- Matrix
